## Laravel 5 Blog (Aplicação de Exemplo) ##

**Laravel 5 Blog (Aplicação de Exemplo)**

### Instalação ###

* `git clone https://gitlab.com/lincoln.coutinho/ExemploLaravel5Blog.git projectname`
* `cd projectname`
* `composer install`
* `php artisan key:generate`
* Create a database and inform *.env*
* `php artisan migrate --seed` to create and populate tables
* Inform *config/mail.php* for email sends
* `php artisan vendor:publish` to publish filemanager
* `php artisan serve` to start the app on http://localhost:8000/

### Features ###

* Home page
* Erro de página personalizada 404
* Autenticação (registo, login, logout, de redefinição de senha, a confirmação mail, acelerador)
* Regras de usuários: administrador (todos os acessos), redator (criar e editar post, carregar e utilizar meios no diretório pessoal), e user (criar comentário no blog)
* Blog com comentários
* Procurar nas mensagens
* Etiquetas em mensagens
* Contacte-nos página
* Painel de administração com mensagens novas, usuários, posts e comentários
* Os usuários admin (filtro de papel, mostrar, editar, apagar, criar)
* As mensagens de administração
* Posts admin (lista com a ordem dinâmica, mostrar, editar, apagar, criar)
* Gestão de Medias
* Localização

### Packages included ###

* laravelcollective/html
* bestmomo/filemanager

### Tricks ###

Usuários para o teste da aplicação :

* Administrator : email = admin@la.fr, password = admin
* Redactor : email = redac@la.fr, password = redac
* User : email = walker@la.fr, password = walker
* User : email = slacker@la.fr, password = slacker
